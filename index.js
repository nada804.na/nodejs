const webServer = require('./common/service/webserver.service.js');
//const database = require('./common/service/database.service.js');
const logger=require('./common/util/logger.util');


async function startup() {

  logger.info('Starting application');
  /* try {
    logger.info('Initializing database module');
    await database.initialize();
  } catch (err) {
    logger.error(err);
    process.exit(1); // Non-zero failure code
  } */

  try {
    logger.info('Initializing web server module');
    await webServer.initialize();
  } catch (err) {
    logger.error(err);
    process.exit(1); // Non-zero failure code
  }
}

startup();

async function shutdown(e) {
  let err = e;
  logger.info('Shutting down application');
  try {
    logger.info('Closing web server module');
    await webServer.close();
  } catch (e) {
    logger.error(e);
    err = err || e;
  }

  /* try {
    logger.info('Closing database module');
    await database.close();
  } catch (e) {
    logger.error(e);
    err = err || e;
  } */
  logger.info('Exiting process');

  if (err) {
    process.exit(1); // Non-zero failure code
  } else {
    process.exit(0);
  }
}

process.on('SIGTERM', () => {
  logger.info('Received SIGTERM');
  shutdown();
});

process.on('SIGINT', () => {
  logger.info('Received SIGINT');
  shutdown();
});
/* handel all Uncaught exceptions *
log it in logger file  */
process.on('uncaughtException', err => {
  //console.log('Uncaught exception');
 // console.error(err);
  logger.error((new Date).toUTCString() + ' uncaughtException:', err.message)
  logger.error(err.stack)
 // shutdown(err);
});
process.on('unhandledRejection', (reason, p) => {
    logger.error(reason, 'Unhandled Rejection at Promise', p);
    //shutdown(reason);
  });