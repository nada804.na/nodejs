const http = require('http');
const express = require('express');
const morgan = require('morgan');
const webServerConfig = require('../config/webserver.config.js');
const bodyParser = require('body-parser')
const OrderRouter = require('../../order/routes.config');
const logger=require('../util/logger.util');
const cors=require('cors');
const BusinessConstants= require('../../common/constant/business.constant')

let httpServer;
function initialize() {
  return new Promise((resolve, reject) => {
    const app = express();
    httpServer = http.createServer(app);

   // Combines logging info from request and response
   app.use(morgan('{"remote_addr": ": remote-addr", "remote_user": ":remote-user", "date": ":date[clf]", "method": ":method", "url": ":url", "http_version": ":http-version", "status": ":status", "result_length": ":res[content-length]", "referrer": ":referrer", "user_agent": ":user-agent", "response_time": ":response-time"}', {stream: logger.stream}));
  
   app.use(cors())
   app.use(bodyParser.json());
   // Mount the router at /api so all its routes start with /api
   OrderRouter.routesConfig(app,'orders');
   
   app.use(function(err, req, res, next) {
      // add this line to include winston logging
     logger.error(`[ORDER_LOG_START] ${err.status || BusinessConstants.INTERNAL_SERVER_ERROR_CODE}, ${err.operationName} Operation
       ${err.serviceName} Service request failed  with Error Code: ${err.status || BusinessConstants.INTERNAL_SERVER_ERROR_CODE}}
        Error Key: ${BusinessConstants.INTERNAL_SERVER_ERROR_KEY} , ${ err.message ||err } [ORDER_LOG_END ,url Request ${req.originalUrl} ,method ${req.method} ,IP ${req.ip}]`)
       /*every response contain code, key(contain erro message)*/
       /*  if(err. != undefined){ */
  /*         err.key=`${BusinessConstants.INTERNAL_SERVER_ERROR_KEY},${ err.message || err}`;
          err.code=BusinessConstants.INTERNAL_SERVER_ERROR_CODE;
       // } */
        let erroObject={
          key:BusinessConstants.INTERNAL_SERVER_ERROR_CODE ,
          code:`${BusinessConstants.INTERNAL_SERVER_ERROR_KEY},${ err.message || err}`
       }
        res.status(BusinessConstants.INTERNAL_SERVER_ERROR_CODE).send(erroObject) 
        
    }); 

   httpServer.listen(webServerConfig.port,webServerConfig.host)
      .on('listening', () => {
        logger.info(`Web server listening on ${webServerConfig.host}:${webServerConfig.port}`);
        resolve();
      })
      .on('error', err => {
        reject(err);
      });
  });
}

module.exports.initialize = initialize;

function close() {
  return new Promise((resolve, reject) => {
    httpServer.close((err) => {
      if (err) {
        reject(err);
        return;
      }

      resolve();
    });
  });
}

module.exports.close = close;

