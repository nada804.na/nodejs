
module.exports = (sequelize, type) => {
    return sequelize.define('USERS', {
        ID : {
          type : type.INTEGER,
          primaryKey : true,
         defaultValue:'TEST_SEQ.nextval',
          //allowNull: false,
          autoIncrement: true,
          // name of colum field:
        },
        USER_NAME : type.STRING,
        IMAGE : type.STRING,
        USER_NUMBER : type.INTEGER,
        POSITION : type.STRING,
        LAST_NAME : type.STRING,
        FIRST_NAME : type.STRING,
        rid: type.INTEGER
    },    
    {
        timestamps : false,
        tableName : 'USERS'      
    })
}